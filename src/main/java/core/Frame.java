package core;

import java.util.ArrayList;
import java.util.List;

public class Frame {
	private List<Integer> score;
	private Frame next;
	private int turn;
	
	public Frame(int turn){
		score = new ArrayList<Integer>();
		next = null;
		this.turn = turn;
	}
	
	public void roll(int pins){
		score.add(pins);		
	}
	
	public boolean isStrike(){
		return (score.size() == 1 && this.pins()==10);
	}
	
	public boolean isSpare(){
		return (score.size() == 2 && this.pins()==10);
	}

	public int pins() {
		int sum = 0;
		for(int p: score){
			sum += p;
		}
		return sum;
	}
	
	public int firstRoll(){
		if (score.size()> 0)
			return score.get(0);
		return 0;
	}
	
	public int score(){
		if(turn>10){ //extra rolls
			return this.pins();
		}
		if(this.isStrike()){
			if(next == null){
				return 10;
			}else if(next.isStrike()){
				if(next.getNext() == null){
					return 20;
				}
				return 20 + next.getNext().firstRoll();
			} else{
				return 10 + next.pins();
			}
		}
		if(this.isSpare()){
			if(next == null){
				return 10;
			}
			return 10 + next.firstRoll();
		}
		return this.pins();
	}
	
	public Frame nextFrame(){
		this.next = new Frame(this.turn + 1);
		return next;
	}
	
	public Frame getNext(){
		return next;
	}
	
	public boolean turnEnded(){
		return (this.isStrike() || (this.score.size() == 2));
	}

	public int getTurn() {
		return this.turn;
	}
}
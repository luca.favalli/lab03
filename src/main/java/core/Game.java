package core;

public class Game{
	private Frame frames;
	private Frame turn;
    
    public Game(){
    	frames = new Frame(1);
    	turn = frames;
    }
    
    public void roll(int pins){
    	turn.roll(pins);
    	if (turn.turnEnded()){
    		turn = turn.nextFrame();
    	}
    }
    
    public int score() {
    	int s = 0;
    	Frame pointer = frames;
    	while(pointer != null && pointer.getTurn() <= 10){
    		s += pointer.score();
    		pointer = pointer.getNext();
    	}
		return s;
	}
    
    public boolean isStrike(){
    	return this.turn.isStrike();
    }
    
    public boolean isSpare(){
    	return this.turn.isSpare();
    }
}
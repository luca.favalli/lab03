package core;

import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BowlingMatch {
	
	public static void main(String[] args) {
		Logger logger = LoggerFactory.getLogger(BowlingMatch.class);
		Random generator = new Random();
		
		int howManyPlayers = generator.nextInt(6) + 1;
		Game[] players = new Game[howManyPlayers];
		for(int i = 0; i < howManyPlayers; i++){
			players[i] = new Game();
		}
		
		//GAME
		for(int i = 0; i < 9; i++){
			logger.info("FRAME {}", i+1);
			for(int j = 0; j<howManyPlayers; j++ ){
				Game player = players[j];
				int pins = generator.nextInt(11);
				player.roll(pins);
				if(!(player.isStrike())){
					player.roll(generator.nextInt(11 - pins));
				}
				logger.info("Player {} - {}", j+1, player.score());
			}
		}
		//LAST FRAME
		logger.info("FRAME 10");
		for(int j = 0; j<howManyPlayers; j++ ){
			Game player = players[j];
			int pins = generator.nextInt(11);
			player.roll(pins);
			if(player.isStrike()){
				pins = generator.nextInt(11);
				player.roll(pins);
				if(player.isStrike())
					player.roll(generator.nextInt(11));
				else player.roll(generator.nextInt(11 - pins));
			} else{
				player.roll(generator.nextInt(11 - pins));
				if(player.isSpare()){
					player.roll(generator.nextInt(11));
				}
			}
			logger.info("Player {} - {}", j+1, player.score());
		}
	}

}
